//
//  APCoreDataManager.swift
//  AdPlayer
//
//  Created by Atul M on 06/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa
import CoreData

class APCoreDataManager: NSObject {
    static let sharedInstance = APCoreDataManager()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        var appDelegate:AppDelegate =  NSApplication.sharedApplication().delegate as! AppDelegate
        return appDelegate.managedObjectContext
    }()
    
    func addCategory(category: APCategory){
        let newCategory = NSEntityDescription.insertNewObjectForEntityForName("Category", inManagedObjectContext: self.managedObjectContext) as! Category

        newCategory.categoryId = category.categoryId!
        newCategory.categoryName = category.categoryName!
        newCategory.categoryImagePath = category.categoryImagePath!
        newCategory.categoryDescription = category.categoryDescription!
        
        do {
            try self.managedObjectContext .save()
        } catch {
            
        }
    }
    
    func deleteCategoryWithId(categoryId: NSInteger){
        let fetchPredicate = NSPredicate(format: "categoryId = %i", categoryId)
        
        let fetchBridge                      = NSFetchRequest(entityName: "Category")
        fetchBridge.predicate                = fetchPredicate
        fetchBridge.returnsObjectsAsFaults   = false
        
        do {
            let fetchedBridges = try self.managedObjectContext.executeFetchRequest(fetchBridge) as! [NSManagedObject]
            
            for fetchedBridge in fetchedBridges {
                self.managedObjectContext.deleteObject(fetchedBridge)
                try self.managedObjectContext.save()
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        deleteVideosForCategory(categoryId)
    }
    
    func deleteVideosForCategory(categoryId: NSInteger){
        let fetchPredicate = NSPredicate(format: "categoryId = %i", categoryId)
        
        let fetchBridge                      = NSFetchRequest(entityName: "Video")
        fetchBridge.predicate                = fetchPredicate
        fetchBridge.returnsObjectsAsFaults   = false
        
        do {
            let fetchedBridges = try self.managedObjectContext.executeFetchRequest(fetchBridge) as! [NSManagedObject]
            
            for fetchedBridge in fetchedBridges {
                self.managedObjectContext.deleteObject(fetchedBridge)
            }
            try self.managedObjectContext.save()
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    func deleteVideo(videoId: NSInteger, categoryId: NSInteger){
        let fetchPredicate = NSPredicate(format: "categoryId = %i AND videoId = %i", categoryId, videoId)
        
        let fetchBridge                      = NSFetchRequest(entityName: "Video")
        fetchBridge.predicate                = fetchPredicate
        fetchBridge.returnsObjectsAsFaults   = false
        
        do {
            let fetchedBridges = try self.managedObjectContext.executeFetchRequest(fetchBridge) as! [NSManagedObject]
            
            for fetchedBridge in fetchedBridges {
                self.managedObjectContext.deleteObject(fetchedBridge)
            }
            try self.managedObjectContext.save()
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }

    
    func addVideo(video: APVideo){
        let newVideo = NSEntityDescription.insertNewObjectForEntityForName("Video", inManagedObjectContext: self.managedObjectContext) as! Video
        newVideo.videoId = video.videoId!
        newVideo.videoPath = video.videoPath!
        newVideo.videoType = video.videoType!
        newVideo.categoryId = video.categoryId!
        newVideo.videoTitle = video.videoTitle!
        newVideo.videoDescription = video.videoDescription!
        do {
            try self.managedObjectContext .save()
        } catch {
            
        }
    }
    
    func categoryList()->[APCategory]{
        let fetchRequest = NSFetchRequest(entityName: "Category")
        let predicate = NSPredicate(value:true)
        fetchRequest.predicate = predicate
        
        var categoryList = [APCategory]()
        
        //3
        do {
            let results =
                try self.managedObjectContext.executeFetchRequest(fetchRequest)
            
            for result in results {
                let categoryObj: Category = result as! Category
                let category : APCategory = APCategory()
                category.categoryId = categoryObj.categoryId
                category.categoryName = categoryObj.categoryName
                category.categoryImagePath = categoryObj.categoryImagePath
                category.categoryDescription = categoryObj.categoryDescription
                categoryList.append(category)
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return categoryList
    }

    func videosInCategory(categoryId: Int)->[APVideo]{
        let fetchRequest = NSFetchRequest(entityName: "Video")
        let predicate = NSPredicate(format:"categoryId = %d", categoryId)
        fetchRequest.predicate = predicate
        
        var videoList = [APVideo]()
        
        //3
        do {
            let results =
                try self.managedObjectContext.executeFetchRequest(fetchRequest)
            
            for result in results {
                let videoObj: Video = result as! Video
                let video : APVideo = APVideo()
                
                video.videoType = videoObj.videoType
                video.videoPath = videoObj.videoPath
                video.videoDescription = videoObj.videoDescription
                video.videoTitle = videoObj.videoTitle
                video.videoId = videoObj.videoId
                video.categoryId = videoObj.categoryId
                
                videoList.append(video)
            }
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return videoList
    }
}
