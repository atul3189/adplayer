//
//  Category+CoreDataProperties.swift
//  AdPlayer
//
//  Created by Atul M on 06/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Category {

    @NSManaged var categoryId: NSNumber?
    @NSManaged var categoryName: String?
    @NSManaged var categoryImagePath: String?
    @NSManaged var categoryDescription: String?

}
