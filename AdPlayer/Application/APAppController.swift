//
//  APAppController.swift
//  AdPlayer
//
//  Created by Atul M on 04/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa

class APAppController: NSObject {
    var window: NSWindow
    var wizard: APWizardViewController?
    init(aWindow: NSWindow) {
        window = aWindow
    }
    
    func appDidLaunch(){
        showSplash()
    }
    
    func showSplash(){
        let splash: APSplashViewController = APSplashViewController(nibName: "APSplashViewController", bundle: nil)!
        window.contentView = splash.view
        self.performSelector(#selector(removeSplashAndLoadApp), withObject: nil, afterDelay: 2)
    }
    
    func removeSplashAndLoadApp() {
        wizard = APWizardViewController(nibName: "APWizardViewController", bundle: nil)!
        window.contentView = wizard!.view
    }
    
    func showWizardRootView(){
        if let wizardObj = self.wizard{
            window.contentView = wizardObj.view
        }
    }
    
}
