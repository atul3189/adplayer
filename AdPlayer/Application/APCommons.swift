//
//  APCommons.swift
//  AdPlayer
//
//  Created by Atul M on 11/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa

class APCommons: NSObject {
    
    static func dialogOKCancel(question: String, text: String) -> Bool {
        let myPopup: NSAlert = NSAlert()
        myPopup.messageText = question
        myPopup.informativeText = text
        myPopup.alertStyle = NSAlertStyle.WarningAlertStyle
        myPopup.addButtonWithTitle("OK")
//        myPopup.addButtonWithTitle("Cancel")
        let res = myPopup.runModal()
        if res == NSAlertFirstButtonReturn {
            return true
        }
        return false
    }

}
