//
//  PlayListManager.swift
//  AdPlayer
//
//  Created by Atul M on 11/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa

class PlayListManager: NSObject {

    static let sharedInstance = PlayListManager()

    var playListItems:[APVideo]?
    
    func loadPlaylistForCategory(categoryId: Int){
        playListItems = APCoreDataManager.sharedInstance.videosInCategory(categoryId)
    }
    
    func addVideoToPlayList(videoPath:NSString){
    
    }
    
}
