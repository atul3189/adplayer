//
//  APVideo.swift
//  AdPlayer
//
//  Created by Atul M on 06/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa

class APVideo: NSObject {
    var videoTitle: String?
    var videoDescription: String?
    var videoId: NSNumber?
    var categoryId: NSNumber?
    var videoType: String?
    var videoPath: String?
}
