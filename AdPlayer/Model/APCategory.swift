//
//  APCategory.swift
//  AdPlayer
//
//  Created by Atul M on 06/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa

class APCategory: NSObject, NSCopying {
    var categoryId: NSNumber?
    var categoryName: String?
    var categoryImagePath: String?
    var categoryDescription: String?
    
    required override init() {
        
    }
    
    required init(category: APCategory) {
        categoryId = category.categoryId
        categoryName = category.categoryName
        categoryImagePath = category.categoryImagePath
        categoryDescription = category.categoryDescription
    }
    
    func copyWithZone(zone: NSZone) -> AnyObject {
        return self.dynamicType.init(category: self)
    }
}
