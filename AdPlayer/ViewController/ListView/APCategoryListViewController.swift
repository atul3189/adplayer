//
//  APCategoryListViewController.swift
//  AdPlayer
//
//  Created by Atul M on 26/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa
let categoryDescriptionTag = 2010
let categoryDeleteTag = 3010
protocol APCategoryListDelegate {
    func didClickOnBackButton()
}

class APCategoryListViewController: NSViewController {

    var categoryList: [APCategory]?
    var delegate: APCategoryListDelegate?
    var videoListViewController: APVideoListViewController?
    
    @IBOutlet weak var categoryTableView: NSTableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NSColor.whiteColor().setFill()
        categoryList = APCoreDataManager.sharedInstance.categoryList()
        // Do view setup here.
    }
    
    @IBAction func backButtonTapped(sender: AnyObject){
        delegate?.didClickOnBackButton()
    }
}


extension APCategoryListViewController : NSTableViewDataSource {
    func numberOfRowsInTableView(tableView: NSTableView) -> Int {
        return categoryList?.count ?? 0
    }
}

extension APCategoryListViewController : NSTableViewDelegate {
    func tableView(tableView: NSTableView, viewForTableColumn tableColumn: NSTableColumn?, row: Int) -> NSView? {
        var cellIdentifier: String = ""
        var categoryName = ""
        var categoryImage = NSImage()
        var categoryDescription = ""
        
        if tableColumn == tableView.tableColumns[0] {
            let category = categoryList![row]
            
            categoryName = category.categoryName!
            categoryImage = NSImage(byReferencingURL: NSURL(string: category.categoryImagePath!)!)
            categoryDescription = category.categoryDescription!
            
            cellIdentifier = "CategoryCell"
        }
        
        if let cell = tableView.makeViewWithIdentifier(cellIdentifier, owner: nil) as? NSTableCellView {
            cell.setAccessibilityIndex(row)
            cell.textField?.stringValue = categoryName
            cell.imageView?.image = categoryImage
            let descriptionLabel = cell.viewWithTag(categoryDescriptionTag) as! NSTextField
            descriptionLabel.stringValue = categoryDescription
            descriptionLabel.toolTip = categoryDescription
            let deleteButton = cell.viewWithTag(categoryDeleteTag) as! NSButton
            deleteButton.target = self
            deleteButton.action = #selector(deleteButtonPressed(_:))
            return cell
        }
        return nil
    }
    
    func deleteButtonPressed(deleteButton: NSButton){
        let selectedIndex = deleteButton.superview?.accessibilityIndex()
        let selectedCategory =  categoryList![selectedIndex!]
        APCoreDataManager.sharedInstance.deleteCategoryWithId((selectedCategory.categoryId?.integerValue)!)
        categoryList =  APCoreDataManager.sharedInstance.categoryList()
        categoryTableView.reloadData()
    }
    
    func tableView(tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        print((categoryList?.count)! + row)
        if(categoryList?.count > row){
            let selectedCategory = categoryList![row]
            videoListViewController = APVideoListViewController()
            videoListViewController?.categoryId = selectedCategory.categoryId?.integerValue
            videoListViewController?.view.frame = self.view.frame
            self.view.addSubview((videoListViewController?.view)!)
            return true
        }
        return false
    }
    
}