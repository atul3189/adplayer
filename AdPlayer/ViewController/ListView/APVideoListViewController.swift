//
//  APVideoListViewController.swift
//  AdPlayer
//
//  Created by Atul M on 29/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa
import Foundation
import AppKit
import AVKit
import AVFoundation

let deleteVideoTag = 5010

class APVideoListViewController: NSViewController {
    @IBOutlet weak var videoListTableView: NSTableView!
    var videoList: [APVideo]?
    var categoryId: Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        videoList = APCoreDataManager.sharedInstance.videosInCategory(categoryId!)
        // Do view setup here.
    }
    
    @IBAction func backButtonClicked(sender: AnyObject){
        self.view.removeFromSuperview()
    }
}

extension APVideoListViewController : NSTableViewDataSource {
    func numberOfRowsInTableView(tableView: NSTableView) -> Int {
        return videoList?.count ?? 0
    }
}

extension APVideoListViewController : NSTableViewDelegate {
    func tableView(tableView: NSTableView, viewForTableColumn tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        var image:NSImage?
        var text:String = ""
        var cellIdentifier: String = ""
        var duration: String = ""
        
        // 1
        guard let item = videoList?[row] else {
            return nil
        }
        
        // 2
        if tableColumn == tableView.tableColumns[0] {
            let movieInfo = extractFirstFrameFromFilepath(item.videoPath!)
            
            if let movieThumb = movieInfo!["image"] {
                image = movieThumb as? NSImage
            }
            if let movieDuration = movieInfo!["duration"]{
                duration = movieDuration as! String
            }
            
            text = item.videoTitle!
            cellIdentifier = "VideoCellId"
        }
        
        // 3
        if let cell = tableView.makeViewWithIdentifier(cellIdentifier, owner: nil) as? NSTableCellView {
            cell.setAccessibilityIndex(row)
            cell.textField?.stringValue = text
            cell.imageView?.image = image ?? nil
            let durationLabel = cell.viewWithTag(cellDurationTag) as! NSTextField
            durationLabel.stringValue = duration
            
            let deleteButton = cell.viewWithTag(deleteVideoTag) as! NSButton
            deleteButton.target = self
            deleteButton.action = #selector(deleteButtonPressed(_:))
            return cell
        }
        return nil
    }
    
    func deleteButtonPressed(deleteButton: NSButton){
        let selectedIndex = deleteButton.superview?.accessibilityIndex()
        let selectedVideo =  videoList![selectedIndex!]
        APCoreDataManager.sharedInstance.deleteVideo((selectedVideo.videoId?.integerValue)!, categoryId: categoryId!)
        videoList = APCoreDataManager.sharedInstance.videosInCategory(categoryId!)
        videoListTableView.reloadData()
    }
    
    func formattedTimeString(totalSeconds: Double)->String{
        let seconds = round(totalSeconds % 60)
        let minutes = round((totalSeconds / 60))
        
        return NSString(format:"%02d:%02d",Int(minutes),Int(seconds)) as String;
    }
    
    func extractFirstFrameFromFilepath(filePath: String)->Dictionary<String,AnyObject>?{
        let fileUrl: NSURL = NSURL (string: filePath)!
        let movieAsset: AVURLAsset = AVURLAsset(URL: fileUrl)
        let movieDuration = movieAsset.duration
        let movieDurationString = formattedTimeString(CMTimeGetSeconds(movieDuration))
        let assetImageGenerator: AVAssetImageGenerator = AVAssetImageGenerator(asset: movieAsset)
        assetImageGenerator.appliesPreferredTrackTransform = true
        do {
            let frameRef: CGImageRef = try assetImageGenerator.copyCGImageAtTime(CMTime(value: 1, timescale: 2), actualTime: nil)
            let image = NSImage(CGImage: frameRef, size: NSSize(width: 50.0, height: 50.0))
            let dict:Dictionary<String,AnyObject> = ["duration":movieDurationString, "image": image];
            return dict
        }
        catch{
            
        }
        return nil
    }
    
    //MARK :- Drag and replace
    
    func tableView(aTableView: NSTableView,
                   writeRowsWithIndexes rowIndexes: NSIndexSet,
                                        toPasteboard pboard: NSPasteboard) -> Bool
    {
        if (aTableView == videoListTableView)
        {
            let registeredTypes:[String] = [NSStringPboardType]
            pboard.declareTypes(registeredTypes, owner: self)
            pboard.setString(String(rowIndexes.firstIndex), forType: "public.data")
            return true
        }
        else
        {
            return false
        }
    }
    
    func tableView(tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableViewDropOperation) -> NSDragOperation {
        if dropOperation == .Above {
            return .Move
        }
        return .None
    }
    
    func tableView(tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableViewDropOperation) -> Bool {
        var oldIndexes = [Int]()
        info.enumerateDraggingItemsWithOptions([], forView: tableView, classes: [NSPasteboardItem.self], searchOptions: [:]) {
            if let str = ($0.0.item as! NSPasteboardItem).stringForType("public.data"), index = Int(str) {
                oldIndexes.append(index)
            }
        }
        
        var oldIndexOffset = 0
        var newIndexOffset = 0
        
        // For simplicity, the code below uses `tableView.moveRowAtIndex` to move rows around directly.
        // You may want to move rows in your content array and then call `tableView.reloadData()` instead.
        tableView.beginUpdates()
        for oldIndex in oldIndexes {
            if oldIndex < row {
                tableView.moveRowAtIndex(oldIndex + oldIndexOffset, toIndex: row - 1)
                let movedVideo = videoList?.removeAtIndex(oldIndex+oldIndexOffset)
                videoList?.insert(movedVideo!, atIndex: row-1)
                oldIndexOffset-=1
            } else {
                tableView.moveRowAtIndex(oldIndex, toIndex: row + newIndexOffset)
                let movedVideo = videoList?.removeAtIndex(oldIndex)
                videoList?.insert(movedVideo!, atIndex: row)
                newIndexOffset+=1
            }
        }
        tableView.endUpdates()
        
        return true
    }
    
}
