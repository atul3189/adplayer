//
//  APSettingsViewController.swift
//  AdPlayer
//
//  Created by Atul M on 04/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa

class APSettingsViewController: NSViewController, NSOpenSavePanelDelegate, NSComboBoxDelegate, NSComboBoxDataSource, APCategoryListDelegate {
    
    @IBOutlet var newCategoryView:NSView!
    @IBOutlet var newVideoView:NSView!
    @IBOutlet var tabView:NSTabView!
    
    @IBOutlet weak var categoryName: NSTextField!
    @IBOutlet var categoryDescription: NSTextView!
    @IBOutlet weak var categoryImage: NSButton!


    @IBOutlet weak var videoCategoryName: NSComboBox!
    @IBOutlet weak var videoTitleField: NSTextField!
    @IBOutlet weak var videoPathField: NSTextField!
    @IBOutlet var videDescriptionField: NSTextView!
    
    var categoryImagePath: String?
    var selectedVideos: [NSString]?
    var categoryListViewController: APCategoryListViewController?
    var windowController: NSWindowController?
    var window: NSWindow?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        setUpTabView()
        setUpAddCategoryView()
        setUpAddVideoView()
    }
    
    func setUpAddCategoryView(){
        
    }
    
    func setUpAddVideoView(){
        let categories = APCoreDataManager.sharedInstance.categoryList()
        let categoryNames = categories.map{$0.categoryName!}
        videoCategoryName.removeAllItems()
        videoCategoryName.addItemsWithObjectValues(categoryNames)
    }
    
    func setUpTabView(){
        let categoryItem = tabView.tabViewItemAtIndex(0)
        categoryItem.view = newCategoryView
        
        let videoItem = tabView.tabViewItemAtIndex(1)
        videoItem.view = newVideoView
    }

    func addCategorySuccess(){
        categoryName.stringValue = ""
        categoryDescription.string = ""
        APCommons.dialogOKCancel("Category added successfully", text: "")
    }
    
    func addVideoSuccess(){
        videoPathField.stringValue = ""
        videoCategoryName.stringValue = ""
        videDescriptionField.string = ""
//        videoTitleField.stringValue = ""
        APCommons.dialogOKCancel("Video added successfully", text: "")
    }
    
    //MARK: - IBAction
    
    @IBAction func saveCategory(sender: AnyObject) {
        
        
        if categoryImagePath == nil || categoryImagePath! == "" || categoryName.stringValue == "" || categoryDescription.string == "" {
            APCommons.dialogOKCancel("Empty Fields", text: "Please complete all fields to continue")
            return
        }
        
        let category:APCategory = APCategory()
        
        category.categoryId = newIdForType("APCategoryId")
        category.categoryImagePath = categoryImagePath
        category.categoryName = categoryName.stringValue
        category.categoryDescription = categoryDescription.string
        
        APCoreDataManager.sharedInstance.addCategory(category)
        addCategorySuccess()
        setUpAddVideoView()
    }
    
    @IBAction func saveVideo(sender: AnyObject) {
        
        if videDescriptionField.string == "" || videoCategoryName.stringValue == "" {
            APCommons.dialogOKCancel("Empty Fields", text: "Please complete all fields to continue")
            return
        }
        var count = 1
        for selectedVideoPath in selectedVideos! {
            let video:APVideo = APVideo()
            video.videoId = newIdForType("APVideoId")
//            video.videoTitle = "\(videoTitleField.stringValue)_\(count)"
            video.videoTitle = NSURL(string: selectedVideoPath as String)?.lastPathComponent
            video.videoDescription = videDescriptionField.string
            
            let selectedCategory = videoCategoryName.stringValue
            let categories = APCoreDataManager.sharedInstance.categoryList()
            var matchingCategoryObjects = categories.filter{$0.categoryName == selectedCategory}
            if matchingCategoryObjects.count > 0{
                video.categoryId = matchingCategoryObjects[0].categoryId
            }
            video.videoPath = selectedVideoPath as String
            video.videoType = "Offline"
            
            APCoreDataManager.sharedInstance.addVideo(video)
            count+=1
        }
        addVideoSuccess()
    }

    
    @IBAction func chooseCategoryImage(sender: AnyObject){
        showPanel()
    }
    
    @IBAction func chooseVideo(sender: AnyObject){
        showPanel()
    }
    
    @IBAction func closeButtonClicked(sender: AnyObject){
        let appDelegate: AppDelegate =  NSApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.appController?.showWizardRootView()
    }
    
    @IBAction func listButtonClicked(sender: AnyObject){
        
        categoryListViewController = APCategoryListViewController(nibName: "APCategoryListViewController", bundle: nil)
        categoryListViewController!.view.frame = self.view.frame
        categoryListViewController?.delegate = self

        
        window = NSWindow(contentViewController: categoryListViewController!)
        window?.title = "Browser"
        windowController = NSWindowController(window: window)
        windowController?.showWindow(window)
    }

    
    //MARK: - Panel Delegates
    
    func showPanel(){
        let panel: NSOpenPanel = NSOpenPanel()
        panel.delegate = self
        panel.canChooseFiles = true
        panel.canChooseDirectories = false
        panel.allowsMultipleSelection = true
        
        panel.beginWithCompletionHandler { (result) in
            if result == NSFileHandlingPanelOKButton{
                if(self.tabView.indexOfTabViewItem(self.tabView.selectedTabViewItem!) == 0){
                    self.handleSelectedCategoryImage(panel.URL!)
                } else {
                    self.handleSelectedVideo(panel.URLs)
                }
            }
        }
    }
    
    func panel(sender: AnyObject, shouldEnableURL url: NSURL) -> Bool {
        let fileName: NSString = url.absoluteString
        let fileFormat: NSString = fileName.pathExtension
        if (fileFormat == "") || (fileFormat == "/") || (fileFormat.length < 1){
            return true
        }
        
        let allowedFormats: NSSet?
        if(tabView.indexOfTabViewItem(tabView.selectedTabViewItem!) == 0){
            allowedFormats = ["png", "jpg"]
        } else {
            allowedFormats = ["mp4", "mov", "avi"]
        }
        return allowedFormats!.containsObject(fileFormat.lowercaseString)
    }
    
    func handleSelectedCategoryImage(fileUrl: NSURL){
        categoryImagePath = fileUrl.absoluteString
        categoryImage.image = NSImage(byReferencingURL: NSURL(string: categoryImagePath!)!)
    }
    
    func handleSelectedVideo(fileUrls: [NSURL]){
        videoPathField.stringValue = "\(fileUrls.count) videos selected."
        selectedVideos = [String]()
        var descriptionText = ""
        let allowedFormats = ["mp4", "mov", "avi"]
        for url in fileUrls {
            if(allowedFormats.contains(url.pathExtension!)){
                descriptionText += "\(url.absoluteString)\n"
                selectedVideos?.append(url.absoluteString)
            }
        }
        videDescriptionField.string = descriptionText
    }
    
    func newIdForType(type: String)->NSNumber{
        
        if let categoryId = NSUserDefaults.standardUserDefaults().objectForKey(type){
            let catId = categoryId as! NSNumber
            NSUserDefaults.standardUserDefaults().setObject(NSNumber(int: catId.intValue+1), forKey: type)
            return NSNumber(int: catId.intValue+1)
        } else {
            NSUserDefaults.standardUserDefaults().setObject(NSNumber(int: 0), forKey: type)
            return (NSNumber(int: 0))
        }
    }
    
    func didClickOnBackButton(){
        if((self.categoryListViewController?.view) != nil){
            self.categoryListViewController?.view.removeFromSuperview()
        }
    }
}
