//
//  APPlayerViewController.swift
//  AdPlayer
//
//  Created by Atul M on 04/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa
import AVFoundation
import AVKit
class APPlayerViewController: NSViewController {

    var videoList: [APVideo]?
    var currentIndex: Int = 0
    var newAdPath: NSString?
    
    @IBOutlet weak var playerView:AVPlayerView?
    
    init?(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?, videoList: [APVideo]?) {
        self.videoList = videoList
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.wantsLayer = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(windowWillClose), name: NSWindowWillCloseNotification, object: nil)
        
        NSEvent.addLocalMonitorForEventsMatchingMask(.KeyDownMask) { (theEvent) -> NSEvent? in
            switch theEvent.keyCode {
            case 53:
                self.windowWillClose()
            default:
                super.keyDown(theEvent)
            }
            return theEvent
        }
        if(videoList?.count > 0){
            playVideoAtIndex(currentIndex)
        }
    }
    
    func windowWillClose(){
        self.view.removeFromSuperview()
        self.playerView?.player?.pause()
    }
    
    func playVideoAtIndex(index: Int){
        // Use a local or remote URL
        let url = NSURL(string: videoList![index].videoPath!)
        
        // Make an AVPlayer
        let avPlayer = AVPlayer(URL: url!)
        avPlayer.actionAtItemEnd = AVPlayerActionAtItemEnd.None
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(APPlayerViewController.playerItemDidReachEnd(_:)),
                                                         name: AVPlayerItemDidPlayToEndTimeNotification,
                                                         object: avPlayer.currentItem)
        
        playerView?.player = avPlayer
        playerView?.showsFullScreenToggleButton = true
        playerView?.controlsStyle = .Floating
        avPlayer.play()
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
        currentIndex+=1
        if(currentIndex >= videoList?.count){
            currentIndex = 0
            windowWillClose()
            return
        }
        playVideoAtIndex(currentIndex)
        
    }
    
    override func cancelOperation(sender: AnyObject?) {
        
    }
    
    deinit{
        
    }
}
