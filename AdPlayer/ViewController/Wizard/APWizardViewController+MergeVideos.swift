//
//  APWizardViewController+MergeVideos.swift
//  AdPlayer
//
//  Created by Atul M on 01/10/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Foundation
import AVFoundation
import Photos

extension APWizardViewController{
    
    @IBAction func mergeVideoArray(sender: AnyObject){
        waitLabel.hidden = false
        let mainComposition = AVMutableComposition()
        let compositionVideoTrack = mainComposition.addMutableTrackWithMediaType(AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
        let compositionAudioTrack = mainComposition.addMutableTrackWithMediaType(AVMediaTypeAudio, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        var insertTime = kCMTimeZero
        
        for video in self.videoList!{
            
            let videoUrl = NSURL(string: video.videoPath!)
            let asset = AVURLAsset(URL: videoUrl!, options: [AVURLAssetPreferPreciseDurationAndTimingKey: "YES"])
            
            let videoAssetTrack = asset.tracksWithMediaType(AVMediaTypeVideo)[0]
            let audioAssetTrack = asset.tracksWithMediaType(AVMediaTypeAudio)[0]
            
            try! compositionVideoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAssetTrack.timeRange.duration), ofTrack: videoAssetTrack, atTime: insertTime)
            try! compositionAudioTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, audioAssetTrack.timeRange.duration), ofTrack: audioAssetTrack, atTime: insertTime)
            
            insertTime = CMTimeAdd(insertTime, videoAssetTrack.timeRange.duration)
        }
        
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0]
        let savePath = (documentDirectory as NSString).stringByAppendingPathComponent("AdPlayer\(NSDate()).mov")
        let url = NSURL(fileURLWithPath: savePath)
        
        let exporter = AVAssetExportSession(asset: mainComposition, presetName: AVAssetExportPresetPassthrough)
        exporter?.outputURL = url
        exporter?.outputFileType = AVFileTypeQuickTimeMovie
        exporter?.exportAsynchronouslyWithCompletionHandler({ 
            self.waitLabel.hidden = true
        })
    }
}



