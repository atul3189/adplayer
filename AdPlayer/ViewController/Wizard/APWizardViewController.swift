//
//  APWizardViewController.swift
//  AdPlayer
//
//  Created by Atul M on 04/09/16.
//  Copyright © 2016 Atul. All rights reserved.
//

import Cocoa

class APWizardViewController: NSViewController, NSOpenSavePanelDelegate, NSTextFieldDelegate, NSTabViewDelegate {

    @IBOutlet var settingsButton: NSButton!
    @IBOutlet weak var videoCategoryName: NSComboBox!
    @IBOutlet weak var videoPathField: NSTextField!
    @IBOutlet weak var tabView: NSTabView!
    @IBOutlet weak var videoListTableView: NSTableView!
    @IBOutlet weak var waitLabel: NSTextField!
    
    var settings:APSettingsViewController?
    var player:APPlayerViewController?
    var helpViewController: APHelpViewController?
    var videoList: [APVideo]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpChooseVideo()
    }
    
    func setUpReArrangeView(){
        let registeredTypes:[String] = [NSStringPboardType]
        videoListTableView.registerForDraggedTypes(registeredTypes)
        videoListTableView.reloadData()
    }
    
    func setUpChooseVideo(){
        videoPathField.delegate = self
        
        let categories = APCoreDataManager.sharedInstance.categoryList()
        let categoryNames = categories.map{$0.categoryName!}
        videoCategoryName.removeAllItems()
        videoCategoryName.addItemsWithObjectValues(categoryNames)
    }
    
    func loadVideoList(){
        videoList?.removeAll()
        
        let categories = APCoreDataManager.sharedInstance.categoryList()
        var matchingCategoryObjects = categories.filter{$0.categoryName == videoCategoryName.stringValue}
        var categoryId: Int?
        if matchingCategoryObjects.count > 0{
            categoryId = matchingCategoryObjects[0].categoryId?.integerValue
        }
        
        videoList = APCoreDataManager.sharedInstance.videosInCategory(categoryId!)
        
        if(videoPathField.stringValue != ""){
            let newVideo = APVideo()
            newVideo.videoTitle = NSURL(string: videoPathField.stringValue)?.lastPathComponent
            newVideo.videoPath = videoPathField.stringValue
            videoList?.insert(newVideo, atIndex: 0)
        }
        
        setUpReArrangeView()
    }
    
    // MARK: - IBAction
    
    @IBAction func settingsButtonTapped(sender: AnyObject) {
        settings = APSettingsViewController(nibName: "APSettingsViewController", bundle: nil)!
        let appDelegate: AppDelegate =  NSApplication.sharedApplication().delegate as! AppDelegate
        appDelegate.window.contentView = settings!.view
    }
    
    @IBAction func chooseVideoButtonTapped(sender: AnyObject){
        showPanel()
    }
    
    @IBAction func nextStepButtonTapped(sender: AnyObject?){
        if(videoCategoryName.stringValue == ""){
            APCommons.dialogOKCancel("Please choose a category to continue", text: "")
            return
        }
        loadVideoList()
        tabView.selectTabViewItem(tabView.tabViewItemAtIndex(1))
    }
    
    @IBAction func playButtonTapped(sender: AnyObject){
        player = APPlayerViewController(nibName: "APPlayerViewController", bundle: nil, videoList: videoList)
        player?.newAdPath = videoPathField.stringValue
        self.player?.view.frame = self.view.frame
        self.view.addSubview((player?.view)!)
//        let appDelegate: AppDelegate =  NSApplication.sharedApplication().delegate as! AppDelegate
//        appDelegate.window.contentView = player!.view
    }
    
    @IBAction func helpButtonTapped(sender: AnyObject){
        helpViewController = APHelpViewController(nibName: "APHelpViewController", bundle: nil)
        self.presentViewControllerAsSheet(helpViewController!)
    }
    
    // MARK: - Choose Video
    
    func showPanel(){
        let panel: NSOpenPanel = NSOpenPanel()
        panel.delegate = self
        panel.canChooseFiles = true
        panel.canChooseDirectories = false
        panel.allowsMultipleSelection = false
        
        panel.beginWithCompletionHandler { (result) in
            if result == NSFileHandlingPanelOKButton{
                self.videoPathField.stringValue = panel.URL!.absoluteString
            }
        }
    }
    
    func panel(sender: AnyObject, shouldEnableURL url: NSURL) -> Bool {
        let fileName: NSString = url.absoluteString
        let fileFormat: NSString = fileName.pathExtension
        if (fileFormat == "") || (fileFormat == "/") || (fileFormat.length < 1){
            return true
        }
        
        let allowedFormats: NSSet?
        allowedFormats = ["mp4", "mov", "avi"]
        return allowedFormats!.containsObject(fileFormat.lowercaseString)
    }
    
    func control(control: NSControl, textShouldBeginEditing fieldEditor: NSText) -> Bool {
        return false
    }
    
    // MARK: - Tab View Delegates
    
    func tabView(tabView: NSTabView, shouldSelectTabViewItem tabViewItem: NSTabViewItem?) -> Bool {
        if(tabView.indexOfTabViewItem(tabViewItem!) == 1){
            if(videoCategoryName.stringValue == ""){
                APCommons.dialogOKCancel("Please choose a category to continue", text: "")
                return false
            }
            loadVideoList()
        }
        return true
    }

}
